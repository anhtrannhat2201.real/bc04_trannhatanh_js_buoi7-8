var numArr = [];
var arrFloat = [];
//hàm lưu trữ số vào mảng
function getNumber() {
  var nhapSo = document.getElementById("inputNum").value * 1;
  document.getElementById("inputNum").value = "";
  numArr.push(nhapSo);
  //   console.log("numArr: ", numArr);
  document.getElementById("txtArray").innerHTML = numArr;
}
//hàm tính tổng các số dương
function calSumPositive() {
  //   console.log("yes");

  var sumPositive = 0;
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum > 0) {
      sumPositive = sumPositive + currentNum;
      //   console.log('sumPositive: ', sumPositive);
    }
    document.getElementById(
      "txtSum"
    ).innerHTML = `Tổng số dương: ${sumPositive}`;
  }
}
//hàm đếm số dương
function countPositive() {
  var countPositive = 0;
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum > 0) {
      countPositive = countPositive + 1;
    }
    document.getElementById("txtCount").innerHTML = `Số dương ${countPositive}`;
  }
}
// hàm tìm số nhỏ nhất
function findMin() {
  var min = numArr[0];
  for (index = 1; index < numArr.length; index++) {
    var currentNum = numArr[index];
    currentNum < min && (min = currentNum);
  }
  document.getElementById("txtMin").innerHTML = "Số nhỏ nhất: " + min;
}
//hàm tính số dương nhỏ nhất
function findMinPos() {
  // console.log("yes");
  var n = [];
  for (i = 0; i < numArr.length; i++) {
    var currentNum1 = numArr[i];
    if (currentNum1 > 0) {
      n.push(currentNum1);
    }
  }
  if (n.length > 0) {
    var minP = n[0];
    for (index = 1; index < n.length; index++) {
      var currentNum2 = n[index];
      if (currentNum2 < minP) {
        minP = currentNum2;
        // console.log('minP: ', minP);
      }
    }
    document.getElementById("txtMinPos").innerHTML =
      "Số dương nhỏ nhất: " + minP;
    // console.log("Số dương nhỏ nhất: ", minP);
  } else {
    document.getElementById("txtMinPos").innerHTML =
      "Không có số dương trong mảng";
    // console.log("Không có số dương trong mảng: ");
  }
}
//hàm tìm số chẵn cuối cùng trong mảng
function findEven() {
  // console.log("yes");

  var result = 0;
  for (index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum % 2 == 0) {
      result = currentNum;
    }
    // console.log('result: ', result);
    document.getElementById("txtEven").innerHTML =
      "Số chẵn cuối cùng: " + result;
  }
}
//hàm sử dụng set chuyển đổi
function chuyenDoi(index1, index2) {
  var result = numArr[index1];
  (numArr[index1] = numArr[index2]), (numArr[index2] = result);
}
//hàm chuyển đổi vị trí
function changePosition() {
  // console.log("yes");
  var inputIndex1 = document.getElementById("inputIndex1").value;
  var inputIndex2 = document.getElementById("inputIndex2").value;
  chuyenDoi(inputIndex1, inputIndex2);

  // console.log("numArr: ", numArr);

  document.getElementById(
    "txtChangePos"
  ).innerHTML = `Mảng sau khi đổi: ${numArr}`;
}
// hàm sử dụng để sắp xếp tăng dần
function sortIncrease() {
  for (var i = 0; i < numArr.length; i++) {
    for (var index = 0; index < numArr.length - 1; index++) {
      var currentNum = numArr[index];
      var currentNum2 = numArr[index + 1];
      currentNum > currentNum2 && chuyenDoi(index, index + 1);
    }
  }

  document.getElementById(
    "txtIncrease"
  ).innerHTML = `Mảng sau khi sắp xếp: ${numArr}`;
}
//start hàm bài 8
//hàm kiểm tra số nguyên tố
function kiemTraSoNguyenTo(n) {
  if (n < 2) {
    return !1;
  }
  for (var i = 2; i <= Math.sqrt(n); i++) {
    if (n % i == 0) {
      return !1;
    }
  }
  return !0;
}
// hàm tìm ra số nguyên tố
function findPrime() {
  // console.log("yes");
  // var n = "không có số nguyên tố";
  var n = -1;
  for (i = 0; i < numArr.length; i++) {
    var currentNum = numArr[i];
    // console.log('currentNum: ', currentNum);
    if (kiemTraSoNguyenTo(currentNum)) {
      //gán currentNum cho n khi xét điều kiện
      n = currentNum;
      // console.log("n: ", n);
      break;
    }
  }
  //output
  document.getElementById("txtPrime").innerHTML = n;
}
//end
//start hàm bài 9
// hàm nhập số
function getFloat() {
  // console.log("yes");
  var num = Number(document.getElementById("inputFloat").value);
  document.getElementById("inputFloat").value = "";
  arrFloat.push(num);
  document.getElementById("txtArrayFloat").innerHTML = arrFloat;
}
// hàm đếm số nguyên
function findInt() {
  var result = 0;
  for (i = 0; i < arrFloat.length; i++) {
    var currentNum = arrFloat[i];
    Number.isInteger(currentNum) && result++;
  }
  document.getElementById("txtInt").innerHTML = `Số nguyên: ${result}`;
}
//end
//hàm so sánh
function compareNum() {
  // console.log("yes");
  var soDuong = 0;
  var soAm = 0;
  var compare = 0;
  for (i = 0; i < numArr.length; i++) {
    var currentNum = numArr[i];
    //sử dụng để đếm số dương và âm
    if (currentNum > 0) {
      soDuong = soDuong + 1;
      // console.log("soDuong: ", soDuong);
    }
    if (currentNum < 0) {
      soAm = soAm + 1;
      // console.log("soAm: ", soAm);
    }
  }
  //sử dụng để so sánh số dương và số âm
  if (soDuong > soAm) {
    compare = "Số dương > Số âm";
  } else if (soDuong < soAm) {
    compare = "Số âm > Số dương";
  } else {
    compare = "Số dương  = Số âm";
  }
  //output
  // console.log("compare: ", compare);
  document.getElementById("txtCompare").innerHTML = compare;
  //
}
